(Transform "AffineTransform")
(NumberOfParameters 6)
(TransformParameters 0.994348 -0.003242 0.007506 0.983135 -1.768293 -1.978869)
(InitialTransformParametersFileName "reg_14//000317_163_s00.log/TransformParameters.0.txt")
(UseBinaryFormatForTransformationParameters "false")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 2)
(MovingImageDimension 2)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 2537 4476)
(Index 0 0)
(Spacing 1.0000000150 1.0000000150)
(Origin 0.0000000000 0.0000000000)
(Direction 1.0000000000 0.0000000000 0.0000000000 1.0000000000)
(UseDirectionCosines "true")

// AdvancedAffineTransform specific
(CenterOfRotationPoint 1108.3554957672 2117.9414237881)

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 3)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "mhd")
(ResultImagePixelType "float")
(CompressResultImage "false")
