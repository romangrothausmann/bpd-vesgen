(Transform "AffineTransform")
(NumberOfParameters 6)
(TransformParameters 1.034635 -0.001406 0.017462 1.006595 16.343330 19.569115)
(InitialTransformParametersFileName "reg_05//000098_050_s01.log/TransformParameters.0.txt")
(UseBinaryFormatForTransformationParameters "false")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 2)
(MovingImageDimension 2)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 5763 8125)
(Index 0 0)
(Spacing 1.0000000150 1.0000000150)
(Origin 0.0000000000 0.0000000000)
(Direction 1.0000000000 0.0000000000 0.0000000000 1.0000000000)
(UseDirectionCosines "true")

// AdvancedAffineTransform specific
(CenterOfRotationPoint 2986.8048173237 4070.4834294113)

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 3)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "mhd")
(ResultImagePixelType "float")
(CompressResultImage "false")
