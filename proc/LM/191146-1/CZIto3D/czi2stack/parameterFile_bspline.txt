(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")

// (UseDirectionCosines "true")

(Registration "MultiResolutionRegistration")
(Interpolator "BSplineInterpolator")
(ResampleInterpolator "FinalBSplineInterpolator")
(Resampler "DefaultResampler")

(FixedImagePyramid "FixedSmoothingImagePyramid")
(MovingImagePyramid "MovingSmoothingImagePyramid")

(Optimizer "AdaptiveStochasticGradientDescent")
// (Optimizer "RegularStepGradientDescent")  // not in SE by default!

(Transform "BSplineTransform")
(BSplineTransformSplineOrder 3) // default 3 (cubic)
(FinalGridSpacingInVoxels  128) // for higest res only, default 8
// (FinalGridSpacingInPhysicalUnits 8) // for higest res only, default 8
// (GridSpacingSchedule 4.0 2.0 1.0 ) // default: power of 2
// (PassiveEdgeWidth 0 1 2)

// (Metric "AdvancedMattesMutualInformation")
(Metric "AdvancedNormalizedCorrelation")
// (Metric "AdvancedMeanSquares")

(AutomaticScalesEstimation "true")

(AutomaticTransformInitialization "true")
// (AutomaticTransformInitializationMethod "GeometricalCenter")
(AutomaticTransformInitializationMethod "CenterOfGravity")
// (AutomaticTransformInitializationMethod "Origins")


(NumberOfResolutions 5)
(ImagePyramidSchedule  256 256  128 128  64 64  32 32 16 16)


(MaximumNumberOfIterations 1000 500 300 200)
(MaximumStepLength   10   5   3   2)       // used by AdaptiveStochasticGradientDescent and RegularStepGradientDescent
// (MinimumStepLength    1 0.5 0.3 0.2)       // only used by RegularStepGradientDescent
// (MinimumGradientMagnitude 0.01 0.01 0.01 0.01) // only used by RegularStepGradientDescent
// (RelaxationFactor 0.99)

(NumberOfSpatialSamples 4000)
(NewSamplesEveryIteration "true") // recommendation is true
// (ImageSampler "Random")
(ImageSampler "RandomSparseMask")  // use to avoid: "Could not find enough image samples within reasonable time. Probably the mask is too small" https://github.com/SuperElastix/elastix/wiki/FAQ#i-am-getting-the-error-message-could-not-find-enough-image-samples-within-reasonable-time-probably-the-mask-is-too-small-what-can-i-do-about-it
(ErodeFixedMask "false")              // use with "Random" sampler to avoid: "Could not find enough image samples within reasonable time. Probably the mask is too small" https://github.com/SuperElastix/SimpleElastix/issues/198#issuecomment-578122776

// (MaximumNumberOfSamplingAttempts 10)
// (ImageSampler "RandomCoordinate")
// (UseRandomSampleRegion "true")
// (SampleRegionSize 50 50)
// (ImageSampler "Full")
// (ImageSampler "Grid") // can be very slow probably due to many samples
// (SampleGridSpacing  2 2  4 4  8 8  16 16) // low res should have low spacing

(BSplineInterpolationOrder 1)
(FinalBSplineInterpolationOrder 3)

(DefaultPixelValue 0)

