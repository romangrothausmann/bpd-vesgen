(Transform "AffineDTITransform")
(NumberOfParameters 7)
(TransformParameters -0.002392 0.023660 0.029330 0.977245 1.071080 25.205579 -3.601487)
(InitialTransformParametersFileName "reg//000008_17-534_0112_s01_01.log/TransformParameters.0.txt")
(UseBinaryFormatForTransformationParameters "false")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 2)
(MovingImageDimension 2)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 5000 5000)
(Index 0 0)
(Spacing 1.0000000150 1.0000000150)
(Origin 0.0000000000 0.0000000000)
(Direction 1.0000000000 0.0000000000 0.0000000000 1.0000000000)
(UseDirectionCosines "true")

// AffineDTITransform specific
(CenterOfRotationPoint 2785.8422203831 2255.7979426866)
(MatrixTranslation 0.9778515712 0.0227792660 0.0310017751 1.0711372702 25.2055792924 -3.6014873406)

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 3)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "mhd")
(ResultImagePixelType "float")
(CompressResultImage "false")
