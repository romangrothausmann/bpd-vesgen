(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")

(DefaultPixelValue 0)

(Registration "MultiResolutionRegistration")
(NumberOfResolutions 4)
(ImagePyramidSchedule 128 128  64 64  32 32  16 16)

(Interpolator "BSplineInterpolator")
(BSplineInterpolationOrder 1)
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 3)

(Resampler "DefaultResampler")
(ImageSampler "Random")
(NumberOfSpatialSamples 4000)

(FixedImagePyramid "FixedSmoothingImagePyramid")
(MovingImagePyramid "MovingSmoothingImagePyramid")

(Optimizer "AdaptiveStochasticGradientDescent")
(MaximumNumberOfIterations 1000 500 300 200)
// (MaximumNumberOfSamplingAttempts 10 15 10)
(AutomaticParameterEstimation "true")
(MaximumStepLength 1 .5 .3 .2) // only used with (AutomaticParameterEstimation "true")


(Metric "AdvancedMeanSquares")


(Transform "AffineDTITransform")
(AutomaticTransformInitialization "true")
(AutomaticTransformInitializationMethod "CenterOfGravity")
